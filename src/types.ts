export interface RootState {
  posts: PostState;
}

export interface Post {
  id: number;
  title: string;
  body: string;
}

export interface PostState {
  posts: Post[];
  loading: boolean;
  hasErrors: boolean;
}

// actions
export const GET_POSTS = "GET_POSTS";
export const GET_POSTS_SUCCESS = "GET_POSTS_SUCCESS";
export const GET_POSTS_FAILURE = "GET_POSTS_FAILURE";

interface GetPostsAction {
  type: typeof GET_POSTS;
}
interface GetPostsSuccess {
  type: typeof GET_POSTS_SUCCESS;
  payload: Post[];
}
interface GetPostsFailure {
  type: typeof GET_POSTS_FAILURE;
}

export type PostActionTypes =
  | GetPostsAction
  | GetPostsSuccess
  | GetPostsFailure;
