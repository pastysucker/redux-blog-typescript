import React from "react";
import { Post } from "../types";

export const PostComponent: React.FunctionComponent<{ post: Post }> = ({
  post,
}) => (
  <article className="post-excerpt">
    <h2>{post.title}</h2>
    <p>{post.body.substring(0, 100)}</p>
  </article>
);
