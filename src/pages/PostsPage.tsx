import React, { useEffect } from "react";
import { connect, ConnectedProps } from "react-redux";
import { PostComponent } from "../components/Post";
import { fetchPosts } from "../actions/postActions";
import { PostState, RootState, PostActionTypes } from "../types";
import { ThunkDispatch } from "redux-thunk";

const mapStateToProps = (state: RootState): PostState => ({
  loading: state.posts.loading,
  posts: state.posts.posts,
  hasErrors: state.posts.hasErrors,
});
const mapDispatchToProps = (
  dispatch: ThunkDispatch<RootState, void, PostActionTypes>
) => {
  return {
    fetchPosts: () => dispatch(fetchPosts()),
  };
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type Props = ConnectedProps<typeof connector>;

const PostsPage: React.FunctionComponent<Props> = ({
  fetchPosts,
  loading,
  posts,
  hasErrors,
}) => {
  useEffect(() => {
    fetchPosts();
  }, [fetchPosts]);

  const renderPosts = () => {
    if (loading) return <p>Loading posts...</p>;
    if (hasErrors) return <p>Unable to display posts.</p>;
    return posts.map((post) => <PostComponent key={post.id} post={post} />);
  };

  return (
    <section>
      <h1>Posts</h1>
      {renderPosts()}
    </section>
  );
};

export default connector(PostsPage);
