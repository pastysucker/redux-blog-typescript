import { PostState, PostActionTypes } from "../types";

export const initialState: PostState = {
  posts: [],
  loading: false,
  hasErrors: false,
};

export default function postsReducer(
  state: PostState = initialState,
  action: PostActionTypes
) {
  switch (action.type) {
    case "GET_POSTS":
      return { ...state, loading: true };
    case "GET_POSTS_SUCCESS":
      return { posts: action.payload, loading: false, hasErrors: false };
    case "GET_POSTS_FAILURE":
      return { ...state, loading: false, hasErrors: true };
    default:
      return state;
  }
}
