import { ThunkDispatch } from "redux-thunk";
import { PostActionTypes, Post, RootState } from "../types";

export const getPosts = (): PostActionTypes => {
  return {
    type: "GET_POSTS",
  };
};

export const getPostsSuccess = (posts: Post[]): PostActionTypes => ({
  type: "GET_POSTS_SUCCESS",
  payload: posts,
});

export const getPostsFailure = (): PostActionTypes => ({
  type: "GET_POSTS_FAILURE",
});

export function fetchPosts() {
  return async (dispatch: ThunkDispatch<RootState, void, PostActionTypes>) => {
    dispatch(getPosts());

    try {
      const response = await fetch(
        "https://jsonplaceholder.typicode.com/posts"
      );
      const data: Post[] = await response.json();

      dispatch(getPostsSuccess(data));
    } catch (error) {
      dispatch(getPostsFailure());
    }
  };
}
